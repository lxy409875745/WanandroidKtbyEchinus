package com.lxy.wanandroidktbyechinus.presenter


import android.content.Context
import android.util.Log
import com.lxy.wanandroidktbyechinus.comment.account.AccountManager
import com.lxy.wanandroidktbyechinus.comment.callback.CallbackKey
import com.lxy.wanandroidktbyechinus.comment.callback.CallbackManager
import com.lxy.wanandroidktbyechinus.comment.callback.ICallback
import com.lxy.wanandroidktbyechinus.comment.json.GsonUtils
import com.lxy.wanandroidktbyechinus.entity.AccountBean
import com.lxy.wanandroidktbyechinus.entity.ResultBean
import com.lxy.wanandroidktbyechinus.constract.LoginConstract
import com.lxy.wanandroidktbyechinus.model.LoginModelImp

class LoginPresenterImp(context: Context) : LoginConstract.ILoginPresenter {


    private val mContext: Context = context

    private lateinit var mView: LoginConstract.ILoginView
    private lateinit var mModelImp: LoginModelImp


    init {
        initModel()
        initCallback()
    }

    //-----------------------------------------对外公开的方法
    /**
     *  登录
     */
    override fun login(userName: String, password: String) {
        if (checkLoginInputData(userName, password)) {
            mModelImp.login(userName, password)
        }
    }


    //----------------------------------------私有方法
    /**
     * 初始化回调
     */
    private fun initCallback() {
        CallbackManager.getInstance().addCallback(CallbackKey.KEY_LOGIN, object : ICallback<Any> { //登录回调
            override fun excuteCallback(value: Any) {
                if (value is Unit) {
                    mView.loginFail("登录失败：未知错误")
                } else if (value is ResultBean<*>) {
                    var resultBean: ResultBean<*> = value

                    Log.i("LoginDelegate", GsonUtils.gson.toJson(resultBean))
                    Log.i("LoginDelegate    data =", GsonUtils.gson.toJson(resultBean.data))

                    if (resultBean.errorCode == 0) { //登录成功
                        mModelImp.saveLoginData(resultBean.data as AccountBean)
                        Log.i("LoginPresenterImp", AccountManager.getAccount().toString())
                        mView.loginSuccess()
                        mView.loginAfter()

                    } else {
                        mView.loginFail(resultBean.errorMsg)
                    }
                }
            }
        })
    }

    /**
     * 检查登录数据是否正确
     */
    private fun checkLoginInputData(userName: String, password: String): Boolean {
        if (userName.isEmpty() ) {
            mView.invalidAccount("用户名不能为空")
            return false
        }
        if (password.isEmpty()) {
            mView.invalidPassword("密码不能为空")
            return false
        }
            return true
        }


        //--------------------------------------初始化


        override fun setView(view: LoginConstract.ILoginView) {
            mView = view
        }

        override fun initModel() {
            mModelImp = LoginModelImp(mContext)
        }
    }