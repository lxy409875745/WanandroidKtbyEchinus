package com.lxy.wanandroidktbyechinus.presenter

import android.content.Context
import com.lxy.wanandroidktbyechinus.adapter.HomeRecycViewAdapter
import com.lxy.wanandroidktbyechinus.comment.callback.CallbackKey
import com.lxy.wanandroidktbyechinus.comment.callback.CallbackManager
import com.lxy.wanandroidktbyechinus.comment.callback.ICallback
import com.lxy.wanandroidktbyechinus.constract.ArticleDataBean
import com.lxy.wanandroidktbyechinus.constract.HomeConstract
import com.lxy.wanandroidktbyechinus.model.HomeModelImp
import com.lxy.wanandroidktbyechinus.utils.NetUtils
import com.youth.banner.Banner

/**
 * @ClassName      HomePresenterImp
 * @Model          todo
 * @Description    home模块presenter
 * @Author          激烈的海胆
 * @Sign            lxy
 * @Date            2018/9/21 11:34
 */
class HomePresenterImp(context: Context?, adapter: HomeRecycViewAdapter, banner: Banner) : HomeConstract.IHomePresenter {
    companion object {
        private var PAGE_INDEX: Int = 0//当前页数
        private const val PAGE_DATA_SIZE = 20//每一页的数据个数

        private var LOREMORE_OVER :Boolean= false//当前已经加载全部数据

    }

    private val mContext: Context = context!!
    private lateinit var mView: HomeConstract.IHomeView
    private lateinit var mModel: HomeConstract.IHomeModel
    private val mAdapter: HomeRecycViewAdapter = adapter//文章列表adapter
    private val mBanner: Banner = banner

    private var isRefresh: Boolean = true//刷新判断
    private var isLoadDataFromLocal: Boolean = false //判断数据是否从本地获取

    init {
        initModel()
        initCallback()
    }

    private fun initCallback() {
        CallbackManager.getInstance().addCallback(CallbackKey.KEY_ARTICLE, object : ICallback<Any> { //----------------------------------首页文章列表 网络获取的数据
            override fun excuteCallback(value: Any) {
                if (value is Unit) {//判空
                    if (isRefresh) {
                        mView.refreshArticleFail("无法获取数据")
                        isRefresh = false
                    } else {
                        mView.loadmoreArticleFail("无法加载数据")
                    }
                    return
                } else if (value is ArticleDataBean) {
                    val dataBean: ArticleDataBean = value
                    val dataIsEmpty = (dataBean.datas == null) //判断当前数据是否为空
                    if (dataIsEmpty) {//处理数据空的场景
                        if (isRefresh) {
                            isRefresh = false
                            mView.refreshArticleFail("刷新数据异常")
                        } else {
                            mView.loadmoreArticleFail("加载数据异常")
                        }
                        return
                    }

                    val netArticleList: List<ArticleDataBean.ArticleBean> = processArticleData(dataBean.datas!!) //数据先经过处理
                    if (isRefresh) {//--------------------------------------------------------------数据刷新
                        val netArticle: ArticleDataBean.ArticleBean = netArticleList[0]
                        val localArticle: ArticleDataBean.ArticleBean? = mModel.getLocalOldestArticle()//获取最久远的一个文章比对

                        if (localArticle != null) { //有本地数据
                            if (netArticle.id == localArticle.id) { //本地数据与网络数据相同 说明未更新数据
                                isLoadDataFromLocal = true
                            } else {
                                isLoadDataFromLocal = false
                                mModel.clearAllLocalArticleData()//清除所有文章的数据 重新保存新的数据
                                mModel.addLocalArticleData(netArticleList)
                            }
                        } else {//本地无数据
                            isLoadDataFromLocal = false
                            mModel.addLocalArticleData(netArticleList)//数据存储到本地
                        }
                        mAdapter.setNewData(netArticleList)//添加处理过的数据
                        isRefresh = false
                        PAGE_INDEX++
                        mView.refreshArticleSuccess(isLoadDataFromLocal)
                    } else {//----------------------------------------------------------------------加载更多
                        LOREMORE_OVER = dataBean.isOver
                        mModel.addLocalArticleData(netArticleList)//数据存储到本地
                        mAdapter.addData(netArticleList)

                        if (! LOREMORE_OVER) {//数据还未加载完
                            PAGE_INDEX++
                        }
                        mView.loadMoreArticleSuccess(dataBean.isOver)
                    }
                }
            }
        })

        CallbackManager.getInstance().addCallback(CallbackKey.KEY_ARTICLE_LOCAL, object : ICallback<Any> { //----------------------------------首页文章列表  本地获取的数据
            override fun excuteCallback(value: Any) {
                val localArticleList: List<ArticleDataBean.ArticleBean>? = value as List<ArticleDataBean.ArticleBean>?
                if (isRefresh) { //------------------------------------刷新
                    if (localArticleList != null && localArticleList.isNotEmpty()) {
                        isLoadDataFromLocal = true
                        mAdapter.setNewData(localArticleList)
                        isRefresh = false
                        mView.refreshArticleSuccess(false)
                        PAGE_INDEX++
                    } else {
                        mModel.requestNetArticle(PAGE_INDEX)//尝试从网络刷新
                    }

                } else {//----------------------------------------------加载
                    if (localArticleList != null && localArticleList.isNotEmpty()) {
                        mAdapter.addData(localArticleList)
                        PAGE_INDEX++
                        mView.loadMoreArticleSuccess(false)
                    } else {
                        mModel.requestNetArticle(PAGE_INDEX)//尝试从网络加载
                    }
                }
            }
        })

    }

    //----------------------------------------私有方法

    /**
     * 处理文章数据 添加必要参数 比如itemtype tag的id等
     */
    private fun processArticleData(articles: List<ArticleDataBean.ArticleBean>): List<ArticleDataBean.ArticleBean> {
        for (index in articles.indices) {
            articles[index].requestTime = System.currentTimeMillis()//设置请求时间
            if (articles[index].tags != null) {//如果當前存在tag 則是項目文章
                articles[index].tags!![0].id = articles[index].id
                articles[index].itemType = ArticleDataBean.ArticleBean.TYPE_PROJECT
            } else {
                articles[index].itemType = ArticleDataBean.ArticleBean.TYPE_NORMAL
            }
        }
        return articles
    }

    //-----------------------------------------公开的方法--------------------------------
    /**
     * 刷新文章
     */

    override fun refreshArticle() {
        isRefresh = true
        PAGE_INDEX = 0//重置页数
        if (NetUtils.isNetworkConnected(mContext)){ //当前有网络连接
            isLoadDataFromLocal = false
            mModel.requestNetArticle(PAGE_INDEX)
        }else{
            mModel.requestLocalArticle(PAGE_INDEX,PAGE_DATA_SIZE)
        }

    }


    /**
     * 加载更多文章
     */
    override fun loadMoreArticle() {
        if (!isRefresh){
            if (isLoadDataFromLocal){ //当前是否从本地在家
                if (LOREMORE_OVER){
                    mView.loadMoreArticleSuccess(true)
                }else{
                    mModel.requestLocalArticle(PAGE_INDEX, PAGE_DATA_SIZE)
                }
            }else{
                if (LOREMORE_OVER){
                    mView.loadMoreArticleSuccess(true)
                }else{
                    mModel.requestNetArticle(PAGE_INDEX)
                }
            }
        }
    }



    /**
     * 获取banner文章
     */
    override fun getBannerArticle() {

    }


    override fun setView(view: HomeConstract.IHomeView) {
        mView = view
    }

    override fun initModel() {
        mModel = HomeModelImp(mContext)
    }
}