package com.lxy.wanandroidktbyechinus.presenter

import android.content.Context
import com.lxy.wanandroidktbyechinus.comment.callback.CallbackKey
import com.lxy.wanandroidktbyechinus.comment.callback.CallbackManager
import com.lxy.wanandroidktbyechinus.comment.callback.ICallback
import com.lxy.wanandroidktbyechinus.constract.RegistConstract
import com.lxy.wanandroidktbyechinus.entity.AccountBean
import com.lxy.wanandroidktbyechinus.entity.ResultBean
import com.lxy.wanandroidktbyechinus.model.RegistModelImp
import java.util.regex.Pattern

class RegistPresenterImp(context: Context) : RegistConstract.IRegistPresenter {

    private val mContext: Context = context
    private lateinit var mView: RegistConstract.IRegistView
    private lateinit var mModel: RegistConstract.IRegistModel

    init {
        initModel()
        initCallback()
    }

    //-----------------------------------------------------------------内部方法----------------------------------------------
    /**
     * 初始化回调
     */
    private fun initCallback() {
        CallbackManager.getInstance().addCallback(CallbackKey.KEY_REGIST, object : ICallback<Any> { //------------注册回调
            override fun excuteCallback(value: Any) {
                if (value is Unit) {
                    mView.registFail("注册失败：未知错误")
                } else if (value is ResultBean<*>) {
                    var resultBean: ResultBean<*> = value
                    if (resultBean.errorCode == 0 && resultBean.data != null) { //注册成功
                        val accountBean: AccountBean = resultBean.data as AccountBean
                        mView.registSuccess()
                        mView.registSuccessAfter()

                        mModel.saveAccount(accountBean)//先保存已经注册的数据
                        mModel.login(accountBean.username, accountBean.password)//登录
                    } else {
                        mView.registFail(resultBean.errorMsg)
                    }
                }
            }
        })

        CallbackManager.getInstance().addCallback(CallbackKey.KEY_LOGIN, object : ICallback<Any> { //---------------登录回调
            override fun excuteCallback(value: Any) {
                if (value is Unit) {
                    mModel.setLoginStatus(false) //未登录未成功
                    mView.loginFail("注册失败：未知错误")
                } else if (value is ResultBean<*>) {
                    var resultBean: ResultBean<*> = value
                    if (resultBean.errorCode == 0 && resultBean.data != null) { //登录成功
                        val accountBean: AccountBean = resultBean.data as AccountBean
                        mModel.setLoginStatus(true)
                        mModel.saveAccount(accountBean)//先保存已经注册的数据

                        mView.loginSuccess()
                        mView.loginSuccessAfter()
                    } else {
                        mModel.setLoginStatus(false) //登录未成功
                        mView.loginFail(resultBean.errorMsg)
                    }
                }
            }
        })
    }
    /**
     * 检查三项输入是否合法
     */
    private fun checkEditextData(userName: String, password: String, rePassword: String): Boolean {

        if (userName.isEmpty()) {//userName判空
            mView.invalidAccount("必须输入用户名")
            return false
        }


        if (isSpecialChar(userName)) {//userName非法字符
            mView.invalidAccount("用户名中不能含有非字母以及非数字的字符")
            return false
        }


        if (password.isEmpty()) {//password判空
            mView.invalidPassword("必须输入密码")
            return false
        }


        if (isSpecialChar(password)) {//password非法字符
            mView.invalidPassword("密码中不能含有非字母以及非数字的字符")
            return false
        }


        if (rePassword.isEmpty()) {//rePassword判空
            mView.invalidRepassword("请再次输入密码")
            return false
        }

        if (isSpecialChar(rePassword)) {//rePassword非法字符
            mView.invalidRepassword("用户名中不能含有非字母以及非数字的字符")
            return false
        }


        if (!password.equals(rePassword)) {//password     rePassword  密码一致性
            mView.invalidRepassword("两次输入的密码不一致")
            return false
        }
        return true
    }

    //--------------------------------------------------------------对外公开的方法--------------------------------------------
    /**
     * 注册
     */
    override fun registAndLogin(userName: String, password: String, rePassword: String) {
        if (checkEditextData(userName, password, rePassword)) { //检查输入合法性
            mModel.regist(userName, password, rePassword)//注册
        }
    }

    /**
     * 设置view
     */
    override fun setView(view: RegistConstract.IRegistView) {
        mView = view
    }

    /**
     * 初始化model
     */
    override fun initModel() {
        mModel = RegistModelImp()
    }


    /**
     * 判断是否含有特殊字符
     *
     * @return true为包含，false为不包含
     */
    fun isSpecialChar(str: String): Boolean {
        val regEx = "[ _`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\n|\r|\t"
        val p = Pattern.compile(regEx)
        val m = p.matcher(str)
        return m.find()
    }
}