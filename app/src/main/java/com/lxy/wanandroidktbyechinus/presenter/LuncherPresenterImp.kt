package com.lxy.wanandroidktbyechinus.presenter

import android.content.Context
import com.lxy.wanandroidktbyechinus.constract.LuncherContract
import com.lxy.wanandroidktbyechinus.model.LuncherModelImp

class LuncherPresenterImp//constructor 构造方法
//如果context为空则抛出空指针异常
(context: Context?) : LuncherContract.Presenter { //如果是单个构造方法可写成这种样式

    private val mContext: Context = context!! // !! 符号 如果传入的context是空则报异常：类似java的if(context != null){mContext = context}else{ throw new NullpointException()}

    private lateinit var mView: LuncherContract.View
    private lateinit var mModel: LuncherContract.Model

    init {
        initModel()//设置mode
    }

    //-----------------------------------------公开的方法

    override fun checkFristOpenStatus() {
        var isFristOpen = mModel.checkFristOpenStatus()
        if (isFristOpen) { //第一次打开
            mModel.setFristOpenFlag()//设置首次打开标识
            mView.onCheckFristOpen(true)

        } else {//不是第一次打开则尝试登陆
            mModel.setFristOpenFlag()//设置首次打开标识
            mView.onCheckFristOpen(true)
            //  mView.onCheckFristOpen(false)
        }
    }

    override fun checkUserLoginStatus() {

    }

    //----------------------------------------初始化
    //初始化model
    override fun initModel() {
        mModel = LuncherModelImp(mContext)
    }

    //初始化view

    override fun setView(view: LuncherContract.View) {
        mView = view
    }


}