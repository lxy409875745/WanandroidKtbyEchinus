package com.lxy.wanandroidktbyechinus.constract

import com.lxy.wanandroidktbyechinus.base.IBaseModel
import com.lxy.wanandroidktbyechinus.base.IBasePresenter
import com.lxy.wanandroidktbyechinus.base.IBaseView
import com.lxy.wanandroidktbyechinus.entity.AccountBean

interface LoginConstract {

    interface ILoginPresenter : IBasePresenter<ILoginView> {
        fun login(userName: String, password: String)//登录

    }


    interface ILoginView : IBaseView {
        fun invalidAccount(errorText: String)//用户名有误
        fun invalidPassword(errorText: String)//密码有误


        fun loginSuccess()//登录成功
        fun loginFail(message:String)//登录失败
        fun loginAfter()//登录之后的操作

    }


    interface ILoginModel : IBaseModel {
        fun login(userName: String, password: String)//登录
        fun saveLoginData(account:AccountBean)//存储登录信息
    }
}