package com.lxy.wanandroidktbyechinus.constract

import com.lxy.wanandroidktbyechinus.base.IBaseModel
import com.lxy.wanandroidktbyechinus.base.IBasePresenter
import com.lxy.wanandroidktbyechinus.base.IBaseView

/**
 * @ClassName      HomeConstract
 * @Model          todo
 * @Description    Home模块constract
 * @Author          激烈的海胆
 * @Sign            lxy
 * @Date            2018/9/21 11:32
 */
interface HomeConstract {
    interface IHomePresenter : IBasePresenter<IHomeView> {
        /**
         * 刷新文章
         */
        fun refreshArticle()
        /**
         * 获取banner文章
         */
        fun getBannerArticle()
        /**
         * 加载更多文章
         */
        fun loadMoreArticle()

    }

    interface IHomeView : IBaseView {
        /**
         * 获取banner文章成功
         */
        fun getBannerArticleSuccess()
        /**
         * 获取banner文章失败
         */
        fun getBannerArticleFail(errorMessage: String)
        /**
         *刷新文章成功
         */
        fun refreshArticleSuccess(haveNewData:Boolean)
        /**
         * 刷新文章失败
         */
        fun refreshArticleFail(errorMessage: String)

        /**
         * 加载文章成功
         */
        fun loadMoreArticleSuccess(isOver: Boolean)
        /**
         * 加载文章失败
         */
        fun loadmoreArticleFail(errorMessage: String)
    }

    interface IHomeModel : IBaseModel {
        /**
         * 从网络获取首页新闻
         */
        fun requestNetArticle(pageIndex:Int)

        /**
         * 从网络获取banner的文章
         */
        fun requestNetBannerArticle()

        //--------------------------------------db操作------------------------------

        /**
         * 从本地获取首页新闻
         */
        fun requestLocalArticle(pageIndex:Int,size:Int)


        /**
         * 从本地获取banner文章
         */
        fun requestLocalBannerArticle()//

        /**
         *添加文章数据
         */
        fun addLocalArticleData(articleData:List<ArticleDataBean.ArticleBean>)

        /**
         * 清除所有首页文章数据
         */
        fun clearAllLocalArticleData()

        /**
         * 获取最久远的一条文章
         */
        fun getLocalOldestArticle(): ArticleDataBean.ArticleBean

    }


}