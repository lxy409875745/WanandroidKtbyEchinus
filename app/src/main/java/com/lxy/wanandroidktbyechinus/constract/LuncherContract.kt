package com.lxy.wanandroidktbyechinus.constract

import com.lxy.wanandroidktbyechinus.base.IBaseModel
import com.lxy.wanandroidktbyechinus.base.IBasePresenter
import com.lxy.wanandroidktbyechinus.base.IBaseView

interface LuncherContract {

    interface View :IBaseView{
        fun onCheckFristOpen(isFristOpen : Boolean)
    }


    interface Presenter :IBasePresenter<View>{
        fun checkFristOpenStatus()//检查是否是第一次打开
        fun checkUserLoginStatus()//检查用户登录状态和登录情况
    }


    interface Model :IBaseModel{
        fun checkFristOpenStatus():Boolean//检查是否是第一次打开 并且修改当前状态
        fun setFristOpenFlag()//第一次打开需要设置已经打开的标识
        fun checkLocalUserData()//检查本地是否保存有用户登录的数据
        fun login()//登录
    }

}