package com.lxy.wanandroidktbyechinus.constract

import com.lxy.wanandroidktbyechinus.base.IBaseModel
import com.lxy.wanandroidktbyechinus.base.IBasePresenter
import com.lxy.wanandroidktbyechinus.base.IBaseView
import com.lxy.wanandroidktbyechinus.entity.AccountBean
import java.lang.Exception

interface RegistConstract {

    interface IRegistView :IBaseView{
        fun invalidAccount(errorText: String)//用户名有误
        fun invalidPassword(errorText: String)//密码有误
        fun invalidRepassword(errorText: String)//再次输入的密码有误

        fun registSuccess()//注册成功
        fun registSuccessAfter()//注册成功后的操作
        fun registFail(errorMessage:String)//注册失败

        fun loginSuccess()//登录成功
        fun loginSuccessAfter()//登录成功后的操作
        fun loginFail(errorMessage: String)//登录失败

    }


    interface IRegistPresenter :IBasePresenter<IRegistView>{
        fun registAndLogin(userName:String,password:String,rePassword:String)
    }

    interface IRegistModel:IBaseModel{
        fun regist(userName:String,password:String,rePassword:String)//注册
        fun login(userName: String,password: String)//登录
        fun saveAccount(account:AccountBean)//保存用户信息
        fun setLoginStatus(isLogin:Boolean)//设置登录状态
    }



}