package com.lxy.wanandroidktbyechinus.comment.network

import com.lzy.okgo.OkGo
import com.lzy.okgo.model.HttpHeaders
import com.lzy.okgo.model.HttpParams
import com.lzy.okgo.request.GetRequest
import com.lzy.okgo.request.PostRequest
import com.lzy.okgo.request.base.Request

class Network {
    companion object {   //kotlin的静态修饰符 类似java的static
        private class HANDLER {
            companion object {
                val INSTANCE: Network = Network()
            }
        }
        fun getInstance(): Network {
            return HANDLER.INSTANCE
        }
    }

    private fun Network(){}

    //--------------------------------------------------get-------------------------------------
     fun get(url:String): Request<String, GetRequest<String>> {
        return OkGo
                .get<String>(url)
                .tag(this)
    }


    fun get(url:String,params: HttpParams): Request<String, GetRequest<String>> {
        return OkGo
                .get<String>(url)
                .tag(this)
                .params(params)
    }

    fun get(url:String,headers: HttpHeaders): Request<String, GetRequest<String>> {
        return OkGo
                .get<String>(url)
                .tag(this)
                .headers(headers)
    }

    fun get(url:String, headers: HttpHeaders, params: HttpParams): Request<String, GetRequest<String>> {
        return OkGo
                .get<String>(url)
                .tag(this)
                .headers(headers)
                .params(params)
    }
    //--------------------------------------------------post-------------------------------------

    fun post(url:String): Request<String, PostRequest<String>> {
        return OkGo
                .post<String>(url)
                .tag(this)
    }

    fun post(url:String,params: HttpParams): Request<String, PostRequest<String>> {
        return OkGo
                .post<String>(url)
                .tag(this)
                .params(params)
    }

    fun post(url:String,headers: HttpHeaders): Request<String, PostRequest<String>> {
        return OkGo
                .post<String>(url)
                .tag(this)
                .headers(headers)
    }

    fun post(url:String, headers: HttpHeaders, params: HttpParams): Request<String, PostRequest<String>> {
        return OkGo
                .post<String>(url)
                .tag(this)
                .headers(headers)
                .params(params)
    }


}