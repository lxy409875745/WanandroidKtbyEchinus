package com.lxy.wanandroidktbyechinus.comment.network

import com.google.gson.reflect.TypeToken
import com.lxy.wanandroidktbyechinus.comment.callback.CallbackKey
import com.lxy.wanandroidktbyechinus.comment.callback.CallbackManager
import com.lxy.wanandroidktbyechinus.comment.json.GsonUtils
import com.lxy.wanandroidktbyechinus.constract.ArticleDataBean
import com.lxy.wanandroidktbyechinus.entity.AccountBean
import com.lxy.wanandroidktbyechinus.entity.ResultBean
import com.lzy.okgo.callback.StringCallback
import com.lzy.okgo.model.HttpParams
import com.lzy.okgo.model.Response
import org.json.JSONException
import org.json.JSONObject

class NetworkHandler {
    companion object {

        //--------------------------------------------------------------公开方法--------------------------------------------
        /**
         * 登录
         */
        fun login(userName: String, password: String) {
            val params = HttpParams()
            params.put("username", userName)
            params.put("password", password)
            Network.getInstance()
                    .post(NetUrlManager.BASE_URL + NetUrlManager.LOGIN, params)
                    .execute(object : StringCallback() {
                        override fun onSuccess(response: Response<String>?) {
                            if (response != null) {
                                if (isJSONValid(response.body())) {
                                    val bean: ResultBean<AccountBean> = GsonUtils.gson.fromJson(response.body(), object : TypeToken<ResultBean<AccountBean>>() {}.type)
                                    excuteCallback(CallbackKey.KEY_LOGIN, bean)
                                } else {
                                    excuteCallback(CallbackKey.KEY_LOGIN, Unit)
                                }
                            } else {
                                excuteCallback(CallbackKey.KEY_LOGIN, Unit)
                            }
                        }

                        override fun onError(response: Response<String>?) {
                            super.onError(response)
                            excuteCallback(CallbackKey.KEY_LOGIN, Unit)
                        }
                    })
        }

        /**
         * 注册
         */
        fun regist(userName: String, password: String, repassword: String) {
            val params = HttpParams()
            params.put("username", userName)
            params.put("password", password)
            params.put("repassword", repassword)
            Network.getInstance()
                    .post(NetUrlManager.BASE_URL + NetUrlManager.REGIST, params)
                    .execute(object : StringCallback() {
                        override fun onSuccess(response: Response<String>?) {
                            if (isJSONValid(response!!.body())) {
                                val bean: ResultBean<AccountBean> = GsonUtils.gson.fromJson(response.body(), object : TypeToken<ResultBean<AccountBean>>() {}.type)
                                excuteCallback(CallbackKey.KEY_REGIST, bean)
                            } else {
                                excuteCallback(CallbackKey.KEY_REGIST, Unit)
                            }
                        }

                        override fun onError(response: Response<String>?) {
                            super.onError(response)
                            excuteCallback(CallbackKey.KEY_REGIST, Unit)
                        }
                    })
        }

        /**
         * 首页文章
         */
        fun article(pageNo: Int) {
            Network.getInstance()
                    .get(NetUrlManager.BASE_URL + NetUrlManager.ARTICLE(pageNo))
                    .execute(object : StringCallback() {
                        override fun onSuccess(response: Response<String>?) {
                            if (isJSONValid(response!!.body())){
                                val bean :ResultBean<ArticleDataBean> = GsonUtils.gson.fromJson(response.body(),object : TypeToken<ResultBean<ArticleDataBean>>() {}.type)
                                excuteCallback(CallbackKey.KEY_ARTICLE,bean)
                            }else{
                                excuteCallback(CallbackKey.KEY_ARTICLE,Unit)
                            }
                        }
                        override fun onError(response: Response<String>?) {
                            excuteCallback(CallbackKey.KEY_ARTICLE,Unit)
                        }

                    })
        }


        //--------------------------------------------------------------内部方法--------------------------------------------
        /**
         * 执行回调
         */
        private fun excuteCallback(key: Any, data: Any) {
            CallbackManager.getInstance().getCallback(key)!!.excuteCallback(data)
        }


        /**
         * 判断是否是json数据类型
         * */
        fun isJSONValid(data: String): Boolean {
            return try {
                JSONObject(data)
                true
            } catch (e: JSONException) {
                false
            }
        }
    }


}