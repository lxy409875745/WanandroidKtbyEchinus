package com.lxy.wanandroidktbyechinus.comment

import com.vondear.rxtool.view.RxToast

class ShowToast {
    companion object {

        fun success(message: String){
            RxToast.success(message)
        }

        fun normal(message:String){
            RxToast.normal(message)
        }

        /**
         * 警告
         */
        fun wranning(message: String){
            RxToast.warning(message)
        }

        /**
         * 错误
         */
        fun error(message: String){
            RxToast.error(message)
        }


    }
}