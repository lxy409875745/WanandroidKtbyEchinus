package com.lxy.wanandroidktbyechinus.comment.account

import android.util.Log
import com.lxy.wanandroidktbyechinus.app.WanandroidApplication
import com.lxy.wanandroidktbyechinus.utils.SharePreferenceUtil


/**
 * 账号持久化管理类
 */
class AccountManager {

    companion object {
        enum class TAG {
            SIGN_STATUS,//登录状态
            USER_DATA//用户数据
        }

        /**
         * 设置登录状态  登录成功后进行设置
         */
        fun setSignStatus(status: Boolean) {
            SharePreferenceUtil.put(WanandroidApplication.getInstance(), TAG.SIGN_STATUS.name, status)

        }

        /**
         * 设置账户信息，登录成功后和setSignStatus方法一起设置
         */
        fun setAccount(userData: String) {
           var isSuccess :Boolean =  SharePreferenceUtil.put(WanandroidApplication.getInstance(), TAG.USER_DATA.name, userData)
        }

        /**
         * 获取账户信息
         */
        fun getAccount(): Any? {
            return SharePreferenceUtil.get(WanandroidApplication.getInstance(), TAG.USER_DATA.name, "")
        }

        /**
         * 检查账号登入状态
         */
        fun checkSignStatus(accountCheckeer:IAccountChecker){
            if (isSignIn() as Boolean){
                accountCheckeer.isSignIn()
            }else{
                accountCheckeer.isNotSignIn()
            }
        }

        /**
         * 判断是否登入
         */
        private fun isSignIn(): Any? {
            return SharePreferenceUtil.get(WanandroidApplication.getInstance(),TAG.SIGN_STATUS.name,false)
        }
    }


    interface IAccountChecker{
        fun isSignIn()//当前已登入
        fun isNotSignIn()//当前未登入
    }

}