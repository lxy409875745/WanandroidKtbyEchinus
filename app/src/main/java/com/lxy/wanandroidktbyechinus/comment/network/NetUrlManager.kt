package com.lxy.wanandroidktbyechinus.comment.network

class NetUrlManager {
    companion object {
        const val BASE_URL = "http://www.wanandroid.com"//接口域名

        const val REGIST = "/user/register"//注册 post

        const val LOGIN = "/user/login" //登录 post

         fun ARTICLE(pageNo :Int):String{//首页文章 pageNo 当前文章页数  get
             return "/article/list/$pageNo/json"
        }
    }

}