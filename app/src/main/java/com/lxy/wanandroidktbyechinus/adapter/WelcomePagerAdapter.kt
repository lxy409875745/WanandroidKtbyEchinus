package com.lxy.wanandroidktbyechinus.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import android.util.SparseArray
import com.lxy.wanandroidktbyechinus.base.BaseFragmentDelegate
import com.lxy.wanandroidktbyechinus.base.BaseGuideFragmentDelegate
import com.lxy.wanandroidktbyechinus.ui.welcome.GuidePageOneDelegate
import com.lxy.wanandroidktbyechinus.ui.welcome.GuidePageThreeDelegate
import com.lxy.wanandroidktbyechinus.ui.welcome.GuidePageTwoDelegate

class WelcomePagerAdapter (fragmentManager: FragmentManager?,parentFragmentDelegate: BaseFragmentDelegate): FragmentPagerAdapter(fragmentManager) {
    private var  mFragmentManager :FragmentManager = fragmentManager!!
    private var fragmentPages : ArrayList<BaseGuideFragmentDelegate> = ArrayList()

    private var idsArray : SparseArray<IntArray> = SparseArray()

    
    init {
        var oneFragment:BaseGuideFragmentDelegate = GuidePageOneDelegate()
        oneFragment.setParentDelegate(parentFragmentDelegate)
        idsArray.put(oneFragment.getFragmentDelegateId(),oneFragment.getChildViewIds())
        Log.i("WelcomePagerAdapter","oneFragment.getChildViewIds()   ------ - "+oneFragment.getChildViewIds().size)

        var twoFragment : BaseGuideFragmentDelegate = GuidePageTwoDelegate()
        twoFragment.setParentDelegate(parentFragmentDelegate)
        idsArray.put(twoFragment.getFragmentDelegateId(),twoFragment.getChildViewIds())
        Log.i("WelcomePagerAdapter","twoFragment.getChildViewIds()  ------ - "+twoFragment.getChildViewIds().size)

        var threeFragment : BaseGuideFragmentDelegate = GuidePageThreeDelegate()
        threeFragment.setParentDelegate(parentFragmentDelegate)
        idsArray.put(threeFragment.getFragmentDelegateId(),threeFragment.getChildViewIds())
        Log.i("WelcomePagerAdapter","threeFragment.getChildViewIds()   ------ - "+threeFragment.getChildViewIds().size)
        fragmentPages.add(oneFragment)
        fragmentPages.add(twoFragment)
        fragmentPages.add(threeFragment)


    }


    fun getIds():SparseArray<IntArray>{
        return idsArray
    }

    override fun getItem(position: Int): Fragment {
        return fragmentPages.get(position)
    }

    override fun getCount(): Int {
        return fragmentPages.size
    }


}