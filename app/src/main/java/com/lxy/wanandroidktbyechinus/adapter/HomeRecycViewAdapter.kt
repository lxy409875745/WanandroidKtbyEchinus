package com.lxy.wanandroidktbyechinus.adapter

import android.content.Context
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.constract.ArticleDataBean

/**
 * @ClassName      HomeRecycViewAdapter
 * @Model          todo
 * @Description    todo
 * @Author          激烈的海胆
 * @Sign            lxy
 * @Date            2018/9/21 11:48
 */
class HomeRecycViewAdapter(context: Context?) : BaseMultiItemQuickAdapter<ArticleDataBean.ArticleBean, BaseViewHolder>(null) {
    private val mContext :Context = context!!
    init {
        addItemType(ArticleDataBean.ArticleBean.TYPE_NORMAL,R.layout.item_article_normal)
        addItemType(ArticleDataBean.ArticleBean.TYPE_PROJECT,R.layout.item_article_project)
    }


    override fun convert(helper: BaseViewHolder?, item: ArticleDataBean.ArticleBean?) {

    }


}