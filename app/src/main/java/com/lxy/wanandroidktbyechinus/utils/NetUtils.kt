package com.lxy.wanandroidktbyechinus.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

/**
 * 网络相关工具类
 */
class NetUtils {

    /**
     * 判断MOBILE网络是否可用
     *
     * @return
     */
    fun isMobileConnected(context: Context?): Boolean {
        if (context != null) {
            val mConnectivityManager = context
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val mMobileNetworkInfo = mConnectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
            if (mMobileNetworkInfo != null) {
                return mMobileNetworkInfo.isAvailable
            }
        }
        return false
    }

    /**
     * 获取当前网络连接的类型信息 1 wifi 2 移动网络 -1 无网络
     *
     * @return
     */
    fun getConnectedType(context: Context?): Int {
        if (context != null) {
            val mConnectivityManager = context
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val mNetworkInfo = mConnectivityManager
                    .activeNetworkInfo
            if (mNetworkInfo != null && mNetworkInfo.isAvailable) {
                return if (mNetworkInfo.type == ConnectivityManager.TYPE_WIFI)
                    1
                else
                    2
            }
        }
        return -1
    }

    companion object {

        /**
         * 判断当前网络是否可用
         *
         * @return
         */
        fun isNetworkConnected(context: Context?): Boolean {
            if (context != null) {
                val mConnectivityManager = context
                        .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                        ?: return false
                val infos = mConnectivityManager.allNetworkInfo
                if (infos != null) {
                    for (info in infos) {
                        if (info.state == NetworkInfo.State.CONNECTED) {
                            return true
                        }
                    }
                }
            }

            return false
        }

        /**
         * 判断当前wifi是否可用
         *
         * @return
         */
        fun isWifiConnected(context: Context?): Boolean {
            if (context != null) {
                val mConnectivityManager = context
                        .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val mWiFiNetworkInfo = mConnectivityManager
                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                if (mWiFiNetworkInfo != null) {
                    return mWiFiNetworkInfo.isAvailable
                }
            }
            return false
        }
    }


}