package com.lxy.wanandroidktbyechinus.utils

import android.content.Context
import android.content.SharedPreferences
import android.os.Parcelable
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method

class SharePreferenceUtil {


    companion object {
        private val SP_FILE_NAME = "LocalData"
        /**
         * 添加某一项
         * @param context 上下文对象
         * @param key  sp所需key值
         * @param value sp所需value值
         * @return
         */
        fun put(context: Context, key: String, value: Any): Boolean {

            val sp = context.getSharedPreferences(SP_FILE_NAME, Context.MODE_PRIVATE)
            val editor = sp.edit()
            if (value is String) {
                editor.putString(key, value)
            } else if (value is Boolean) {
                editor.putBoolean(key, value)
            } else if (value is Int) {
                editor.putInt(key, value)
            } else if (value is Float) {
                editor.putFloat(key, value)
            } else if (value is Long) {
                editor.putLong(key, value)
            } else if (value is Parcelable) {

            } else {
                editor.putString(key, value.toString())
            }
            return editor.commit()
        }

        /**
         * 获取某一项
         * @param context
         * @param key
         * @param defaultObject
         * @return
         */
        operator fun get(context: Context, key: String, defaultObject: Any): Any? {
            val sp = context.getSharedPreferences(SP_FILE_NAME,
                    Context.MODE_PRIVATE)
            if (defaultObject is String) {
                return sp.getString(key, defaultObject)
            } else if (defaultObject is Int) {
                return sp.getInt(key, defaultObject)
            } else if (defaultObject is Boolean) {
                return sp.getBoolean(key, defaultObject)
            } else if (defaultObject is Float) {
                return sp.getFloat(key, defaultObject)
            } else if (defaultObject is Long) {
                return sp.getLong(key, defaultObject)
            }
            return null
        }

        /**
         * 移除某一项
         * @param context
         * @param key
         * @return
         */

        fun remove(context: Context, key: String): Boolean {
            val sp = context.getSharedPreferences(SP_FILE_NAME, Context.MODE_PRIVATE)
            val editor = sp.edit()
            return editor.remove(key).commit()
        }

        /**
         * 清除所有
         * @param context
         * @return
         */
        fun clearAll(context: Context): Boolean {
            val sp = context.getSharedPreferences(SP_FILE_NAME, Context.MODE_PRIVATE)
            return sp.edit().clear().commit()
        }

        /**
         * 查询某个key是否已经存在
         *
         * @param context
         * @param key
         * @return
         */
        fun contains(context: Context, key: String): Boolean {
            val sp = context.getSharedPreferences(SP_FILE_NAME,
                    Context.MODE_PRIVATE)
            return sp.contains(key)
        }

        /**
         * 返回所有的键值对
         *
         * @param context
         * @return
         */
        fun getAll(context: Context): Map<String, *> {
            val sp = context.getSharedPreferences(SP_FILE_NAME,
                    Context.MODE_PRIVATE)
            return sp.all
        }


        /** 如果使用apply的话
         * 创建一个解决SharedPreferencesCompat.apply方法的一个兼容类
         *
         * @author zhy
         */
        private object SharedPreferencesCompat {
            private val sApplyMethod = findApplyMethod()

            /**
             * 反射查找apply的方法
             *
             * @return
             */
            private fun findApplyMethod(): Method? {
                try {
                    val clz = SharedPreferences.Editor::class.java
                    return clz.getMethod("apply")
                } catch (e: NoSuchMethodException) {
                }

                return null
            }

            /**
             * 如果找到则使用apply执行，否则使用commit
             *
             * @param editor
             */
            fun apply(editor: SharedPreferences.Editor) {
                try {
                    if (sApplyMethod != null) {
                        sApplyMethod.invoke(editor)
                        return
                    }
                } catch (e: IllegalArgumentException) {
                } catch (e: IllegalAccessException) {
                } catch (e: InvocationTargetException) {
                }

                editor.commit()
            }
        }
    }

}