package com.lxy.wanandroidktbyechinus.ui.main.project

import android.os.Bundle
import android.view.View
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.base.BaseFragmentDelegate
import com.lxy.wanandroidktbyechinus.ui.main.navigation.NavigationDelegate

/**
 * @ClassName      ProjectDelegate
 * @Model          todo
 * @Description    todo
 * @Author          激烈的海胆
 * @Sign            lxy
 * @Date            2018/9/19 10:28
 */
class ProjectDelegate :BaseFragmentDelegate() {


    companion object {
        fun creat(): ProjectDelegate {
            return ProjectDelegate()
        }
    }


    override fun onBindView(rootView: View, savedInstanceState: Bundle?) {

    }

    override fun setLayout(): Any {
        return R.layout.delegate_project
    }
}