package com.lxy.wanandroidktbyechinus.ui.welcome

import android.os.Bundle
import android.view.View
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.base.BaseGuideFragmentDelegate
import me.yokeyword.fragmentation.ISupportFragment

class GuidePageTwoDelegate : BaseGuideFragmentDelegate() {



    override fun onBindView(rootView: View, savedInstanceState: Bundle?) {
    }

    override fun getChildViewIds(): IntArray {
        return idsArray
    }

    override fun setLayout(): Any {
        return R.layout.delegate_guide_page_two
    }

    override fun getFragmentDelegateId(): Int {
        return R.layout.delegate_guide_page_two
    }

    override fun setParentDelegate(parentDelegate: ISupportFragment) {

    }
}