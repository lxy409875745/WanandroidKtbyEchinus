package com.lxy.wanandroidktbyechinus.ui.main


import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.base.BaseFragmentDelegate
import com.lxy.wanandroidktbyechinus.base.BaseQuitFragmentDelegate
import com.lxy.wanandroidktbyechinus.comment.ShowToast
import com.lxy.wanandroidktbyechinus.ui.main.home.HomeDelegate
import com.lxy.wanandroidktbyechinus.ui.main.knowledge.KnowledgeDelegate
import com.lxy.wanandroidktbyechinus.ui.main.navigation.NavigationDelegate
import com.lxy.wanandroidktbyechinus.ui.main.project.ProjectDelegate
import com.lxy.wanandroidktbyechinus.widget.bottomnavigationview.BottomNavigationViewHelper

class MainDelegate : BaseQuitFragmentDelegate() {
    private val FIRST = 0
    private val SECOND = 1
    private val THIRD = 2
    private val FOUR = 3

    private lateinit var mBottomNavigationView: BottomNavigationView //底部导航栏
    private lateinit var mContentContainer: ViewGroup //容器

    private val ITEM_DELEGATES: ArrayList<BaseFragmentDelegate> = ArrayList()
    private var selectedIndex: Int = 0//当前选中的item

    companion object {
        fun getInstance(): MainDelegate {
            return MainDelegate()
        }
    }
    //------------------------------------------------------------------公开方法-----------------------------------------------
    override fun onBindView(rootView: View, savedInstanceState: Bundle?) {
        initViews()
        initItem()
    }

    override fun setLayout(): Any {
        return R.layout.delegate_main
    }

    //------------------------------------------------------------------私有方法-----------------------------------------------
    /**
     * 初始化item
     */
    private fun initItem() {
        ITEM_DELEGATES.add(HomeDelegate.creat())
        ITEM_DELEGATES.add(KnowledgeDelegate.creat())
        ITEM_DELEGATES.add(NavigationDelegate.creat())
        ITEM_DELEGATES.add(ProjectDelegate.creat())

        selectedIndex = FIRST

        supportDelegate.loadMultipleRootFragment(
                R.id.delegate_main_content_container,
                selectedIndex, ITEM_DELEGATES[FIRST],
                ITEM_DELEGATES[SECOND],
                ITEM_DELEGATES[THIRD],
                ITEM_DELEGATES[FOUR])
    }

    private fun initViews() {
        //底部导航栏
        mBottomNavigationView = bind(R.id.delegate_main_bottomNavgationView)
        BottomNavigationViewHelper.disableShiftMode(mBottomNavigationView)//去除shift效果
        mBottomNavigationView.setOnNavigationItemSelectedListener { item ->
            selectItem(item.itemId)
            false
        }

        mContentContainer = bind(R.id.delegate_main_content_container)//容器
    }

    private fun selectItem(itemId:Int){
        when(itemId){
            R.id.bottom_nav_item_home ->{//首页
                if (selectedIndex != FIRST){
                    supportDelegate.showHideFragment(ITEM_DELEGATES[FIRST],ITEM_DELEGATES[selectedIndex])
                    selectedIndex = FIRST
                }
            }
            R.id.bottom_nav_item_knowledge->{//知识体系
                if (selectedIndex != SECOND){
                    supportDelegate.showHideFragment(ITEM_DELEGATES[SECOND],ITEM_DELEGATES[selectedIndex])
                    selectedIndex = SECOND
                }
            }
            R.id.bottom_nav_item_navigation->{//导航
                if (selectedIndex != THIRD){
                    supportDelegate.showHideFragment(ITEM_DELEGATES[THIRD],ITEM_DELEGATES[selectedIndex])
                    selectedIndex = THIRD
                }
            }
            R.id.bottom_nav_item_project->{//项目
                if (selectedIndex != FOUR){
                    supportDelegate.showHideFragment(ITEM_DELEGATES[FOUR],ITEM_DELEGATES[selectedIndex])
                    selectedIndex = FOUR
                }
            }
        }
    }
}