package com.lxy.wanandroidktbyechinus.ui.main.navigation

import android.os.Bundle
import android.view.View
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.base.BaseFragmentDelegate
import com.lxy.wanandroidktbyechinus.ui.main.knowledge.KnowledgeDelegate

/**
 * @ClassName      NavigationDelegate
 * @Model          todo
 * @Description    todo
 * @Author          激烈的海胆
 * @Sign            lxy
 * @Date            2018/9/19 10:27
 */
class NavigationDelegate :BaseFragmentDelegate() {


    companion object {
        fun creat(): NavigationDelegate {
            return NavigationDelegate()
        }
    }


    override fun onBindView(rootView: View, savedInstanceState: Bundle?) {

    }

    override fun setLayout(): Any {
       return R.layout.delegate_navigation
    }
}