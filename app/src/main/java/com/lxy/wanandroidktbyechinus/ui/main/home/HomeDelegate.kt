package com.lxy.wanandroidktbyechinus.ui.main.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.adapter.HomeRecycViewAdapter
import com.lxy.wanandroidktbyechinus.base.BaseFragmentDelegate
import com.lxy.wanandroidktbyechinus.constract.HomeConstract
import com.lxy.wanandroidktbyechinus.presenter.HomePresenterImp
import com.lxy.wanandroidktbyechinus.widget.banner.BannerImageLoader
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.youth.banner.Banner

/**
 * @ClassName      HomeDelegate
 * @Model          todo
 * @Description    todo
 * @Author          激烈的海胆
 * @Sign            lxy
 * @Date            2018/9/19 10:27
 */
class HomeDelegate : BaseFragmentDelegate() ,HomeConstract.IHomeView{


    private lateinit var mRefreshLayout:SmartRefreshLayout
    private lateinit var mRecycView:RecyclerView
    private lateinit var mAdapter:HomeRecycViewAdapter
    private lateinit var mPresenter:HomePresenterImp
    private lateinit var mBanner:Banner

    companion object {
        fun creat():HomeDelegate{
            return HomeDelegate()
        }
    }

    override fun onBindView(rootView: View, savedInstanceState: Bundle?) {
        initViews()
        initPresenter()
    }


    override fun setLayout(): Any {
        return R.layout.delegate_home
    }

    //------------------------------------------------------私有方法---------------------------------------

    private fun initViews(){
        initBanner()
        initRecycview()

    }



    private fun initBanner(){
        mBanner= LayoutInflater.from(context).inflate(R.layout.item_banner,null,false) as Banner
        mBanner.setImageLoader(BannerImageLoader())
        mBanner.setOnBannerListener { //banner点击事件

        }

    }

    private fun initRecycview(){
        mRefreshLayout = bind(R.id.delegate_home_smartRefreshLayout)
        mRefreshLayout.setOnRefreshListener {
            mPresenter.refreshArticle()
            mPresenter.getBannerArticle()
        }
        mRecycView = bind(R.id.delegate_home_recycView)
        mRecycView.layoutManager = LinearLayoutManager(context)
        mAdapter = HomeRecycViewAdapter(context)
        mAdapter.addHeaderView(mBanner)
        mAdapter.setOnLoadMoreListener({ mPresenter.loadMoreArticle() },mRecycView)
        mRecycView.adapter = mAdapter
    }
    private fun initPresenter(){
        mPresenter = HomePresenterImp(context,mAdapter,mBanner)
    }

    //----------------------------------------------------IHomeView公开方法------------------------------------------
    /**
     * 获取banner文章成功
     */
    override fun getBannerArticleSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * 获取banner文章失败
     */
    override fun getBannerArticleFail(errorMessage: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * 刷新文章成功
     */
    override fun refreshArticleSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * 刷新文章失败
     */
    override fun refreshArticleFail(errorMessage: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * 加载更多文章成功
     */
    override fun loadMoreArticleSuccess(haveData: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * 加载更多文章失败
     */
    override fun loadmoreArticleFail(errorMessage: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}