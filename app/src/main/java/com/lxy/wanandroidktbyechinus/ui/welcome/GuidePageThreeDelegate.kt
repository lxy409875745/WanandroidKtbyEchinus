package com.lxy.wanandroidktbyechinus.ui.welcome

import android.os.Bundle
import android.view.View
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.base.BaseGuideFragmentDelegate
import com.lxy.wanandroidktbyechinus.ui.login.LoginDelegate
import com.lxy.wanandroidktbyechinus.ui.main.MainDelegate
import com.lxy.wanandroidktbyechinus.ui.regist.RegistDelegate
import kotlinx.android.synthetic.main.delegate_guide_page_three.*
import me.yokeyword.fragmentation.ISupportFragment

class GuidePageThreeDelegate : BaseGuideFragmentDelegate() {

    private lateinit var mLoginBtn: View//登录
    private lateinit var mRegistBtn: View//注册
    private lateinit var mPassBtn:View//随便逛逛


    override fun onBindView(rootView: View, savedInstanceState: Bundle?) {
        initBtn()
    }

    override fun getChildViewIds(): IntArray {
        return idsArray
    }

    private fun initBtn() {
        mLoginBtn = bind(R.id.welcome_item_login_btn_2)
        mLoginBtn.setOnClickListener {
            with(mParentDelegate) {
                //登录
                supportDelegate.startWithPop(LoginDelegate())

            }
        }

        mRegistBtn = bind(R.id.welcome_item_regist_btn_2)
        mRegistBtn .setOnClickListener {
            //注册
            with(mParentDelegate) {
                supportDelegate.startWithPop(RegistDelegate())

            }
        }
        mPassBtn = bind(R.id.welcome_item_pass_btn_2)
        mPassBtn.setOnClickListener {
            //随便逛逛
            with(mParentDelegate) {
                supportDelegate.startWithPop(MainDelegate())
            }
        }

    }


    override fun setLayout(): Any {
        return R.layout.delegate_guide_page_three
    }


    override fun getFragmentDelegateId(): Int {
        return R.layout.delegate_guide_page_three
    }


    override fun setParentDelegate(parentDelegate: ISupportFragment) {
        mParentDelegate = parentDelegate
    }
}