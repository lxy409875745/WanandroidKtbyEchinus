package com.lxy.wanandroidktbyechinus.ui.main.knowledge

import android.os.Bundle
import android.view.View
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.base.BaseFragmentDelegate
import com.lxy.wanandroidktbyechinus.ui.main.home.HomeDelegate

/**
 * @ClassName      KnowledgeDelegate
 * @Model          todo
 * @Description    todo
 * @Author          激烈的海胆
 * @Sign            lxy
 * @Date            2018/9/19 10:27
 */
class KnowledgeDelegate :BaseFragmentDelegate() {

    companion object {
        fun creat(): KnowledgeDelegate {
            return KnowledgeDelegate()
        }
    }

    override fun onBindView(rootView: View, savedInstanceState: Bundle?) {

    }

    override fun setLayout(): Any {
        return R.layout.delegate_knowledge
    }
}