package com.lxy.wanandroidktbyechinus.ui.luncher

import android.os.Bundle
import android.view.View
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.base.BaseQuitFragmentDelegate
import com.lxy.wanandroidktbyechinus.constract.LuncherContract
import com.lxy.wanandroidktbyechinus.presenter.LuncherPresenterImp
import com.lxy.wanandroidktbyechinus.ui.welcome.WelcomeDelegate

class LuncherDelegate : BaseQuitFragmentDelegate(), LuncherContract.View {

    private lateinit var mPresenter: LuncherPresenterImp

    override fun onBindView(rootView: View, savedInstanceState: Bundle?) {
        initPresenter()
        mPresenter.checkFristOpenStatus()//检查是否是第一次打开的状态
    }

    private fun initPresenter() {
        mPresenter = LuncherPresenterImp(context)
        mPresenter.setView(this)
    }

    override fun setLayout(): Any {
        return R.layout.delegate_luncher
    }

    override fun onCheckFristOpen(isFristOpen: Boolean) {
        if (isFristOpen) { //引导过渡页
            start(WelcomeDelegate())
        } else {
            //do something
            start(WelcomeDelegate())

        }
    }

}