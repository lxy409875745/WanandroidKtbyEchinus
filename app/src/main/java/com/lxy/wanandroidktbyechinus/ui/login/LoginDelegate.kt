package com.lxy.wanandroidktbyechinus.ui.login

import android.os.Build
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.base.BaseQuitFragmentDelegate
import com.lxy.wanandroidktbyechinus.comment.ShowToast
import com.lxy.wanandroidktbyechinus.constract.LoginConstract
import com.lxy.wanandroidktbyechinus.presenter.LoginPresenterImp
import com.lxy.wanandroidktbyechinus.ui.main.MainDelegate
import com.lxy.wanandroidktbyechinus.ui.regist.RegistDelegate
import com.lxy.wanandroidktbyechinus.widget.SharedElementTransation
import kotlinx.android.synthetic.main.delegate_login.*

class LoginDelegate : BaseQuitFragmentDelegate(), LoginConstract.ILoginView {
    private lateinit var mPresenter: LoginPresenterImp
    private lateinit var mLoginBtn :View //登录按钮
    private lateinit var mOpenRegistBtn:View//打开注册界面
    private lateinit var mPassBtn:View //跳过按钮

    private lateinit var mUserNameTextInputLayout:TextInputLayout//账号输入textinputlayout
    private lateinit var mPasswordTextInputLayout:TextInputLayout

    private lateinit var mUserNameEdt :EditText//用户名输入框
    private lateinit var mPasswordEdt :EditText//密码输入框


    override fun onBindView(rootView: View, savedInstanceState: Bundle?) {
        initViews()
        initPresenter()
        mUserNameEdt.setText("lxy409875745")
        mPasswordEdt.setText("lxy19941128")
    }


    private fun initPresenter() {
        mPresenter = LoginPresenterImp(context!!)
        mPresenter.setView(this)
    }


    private fun initViews() {
        //登录
        mLoginBtn = bind(R.id.delegate_login_start_login_btn)
        mLoginBtn.setOnClickListener {
            clearAccountErrorMessage()
            clearPasswordErrorMessage()
            val userName = delegate_login_account_edt.text.toString()
            val password = delegate_login_password_edt.text.toString()
            getBaseActivity().showLoading(false)
            mPresenter.login(userName, password)

        }


        //跳过直接打开
        mPassBtn = bind(R.id.delegate_login_pass_btn)
        mPassBtn.setOnClickListener {
            supportDelegate.startWithPop(MainDelegate.getInstance())
        }

        //打开注册界面
        mOpenRegistBtn = bind(R.id.delegate_login_start_regist_btn)
        mOpenRegistBtn.setOnClickListener {
            startRegistDelegate()
        }

        //textinputLayout
        mUserNameTextInputLayout = bind(R.id.delegate_login_account_textInputLayout)
        mPasswordTextInputLayout = bind(R.id.delegate_login_password_textInputLayout)


        //输入框
        mUserNameEdt = bind(R.id.delegate_login_account_edt)
        mUserNameEdt.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                clearAccountErrorMessage()
            }
        })

        mPasswordEdt = bind(R.id.delegate_login_password_edt)
        mPasswordEdt.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                clearPasswordErrorMessage()
            }

        })

    }

    /**
     * 打开注册页面
     */
    private fun startRegistDelegate(){
        val registDelegate = RegistDelegate()

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {

            registDelegate.sharedElementReturnTransition = SharedElementTransation()
            registDelegate.sharedElementEnterTransition = SharedElementTransation()

            // 25.1.0以下的support包,Material过渡动画只有在进栈时有,返回时没有;
            // 25.1.0+的support包，SharedElement正常

            supportDelegate.extraTransaction()
                    .addSharedElement(mLoginBtn, getString(R.string.transation_regist_btn))
                    .startWithPop(registDelegate)
        } else {
           supportDelegate.startWithPop(registDelegate)
        }
    }


    /**
     * 清除用户名错误提示
     */
    private fun clearAccountErrorMessage(){
        mUserNameTextInputLayout.error = ""
      //  mUserNameTextInputLayout.isErrorEnabled = false
    }

    /**
     * 清除密码错误提示
     */
    private fun clearPasswordErrorMessage(){
        mPasswordTextInputLayout.error = ""
      //  mPasswordTextInputLayout.isErrorEnabled = false
    }


    //---------------------------------------------公开的方法

    /**
     * 账号有误
     */
    override fun invalidAccount(errorText: String) {
        getBaseActivity().stopLoading()
       // mUserNameTextInputLayout.isErrorEnabled = true
        mUserNameEdt.error =  errorText
    }

    /**
     * 密码有误
     */
    override fun invalidPassword(errorText: String) {
        getBaseActivity().stopLoading()
      //  mPasswordTextInputLayout.isErrorEnabled = true
        mPasswordEdt.error = errorText
    }

    /**
     * 登录成功
     */
    override fun loginSuccess() {
        getBaseActivity().stopLoading()
        ShowToast.success("登录成功!")
    }

    /**
     * 登录成功后的操作
     */
    override fun loginAfter() {
        supportDelegate.startWithPop(MainDelegate.getInstance())
    }

    /**
     * 登录失败
     */
    override fun loginFail(message: String) {
        getBaseActivity().stopLoading()
        ShowToast.error("登录失败：$message")
    }


    override fun setLayout(): Any {
        return R.layout.delegate_login
    }
}