package com.lxy.wanandroidktbyechinus.ui.regist

import android.os.Build
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.base.BaseQuitFragmentDelegate
import com.lxy.wanandroidktbyechinus.comment.ShowToast
import com.lxy.wanandroidktbyechinus.constract.RegistConstract
import com.lxy.wanandroidktbyechinus.presenter.RegistPresenterImp
import com.lxy.wanandroidktbyechinus.ui.login.LoginDelegate
import com.lxy.wanandroidktbyechinus.ui.main.MainDelegate
import com.lxy.wanandroidktbyechinus.widget.SharedElementTransation

class RegistDelegate : BaseQuitFragmentDelegate(), RegistConstract.IRegistView {



    private lateinit var mRegistBtn: View
    private lateinit var mPassBtn: View
    private lateinit var mOpenLoginBtn: View

    private lateinit var mUserNameEdt: EditText//用户名输入框
    private lateinit var mPasswordEdt: EditText//密码输入框
    private lateinit var mRepasswordEdt: EditText//再次输入密码

    private lateinit var mUserNameTextInputLayout: TextInputLayout
    private lateinit var mPasswordTextInputLayout: TextInputLayout
    private lateinit var mRepasswordTextInputLayout: TextInputLayout


    private lateinit var mPresenter: RegistConstract.IRegistPresenter

    override fun onBindView(rootView: View, savedInstanceState: Bundle?) {
        initViews()
        initPresenter()
    }


    //-------------------------------------------------------------------------------内部方法------------------------------------------------
    private fun initPresenter() {
        mPresenter = RegistPresenterImp(context!!)
        mPresenter.setView(this)
    }

    private fun initViews() {
        initBtnViews()
        initEdtViews()
    }

    /**
     * 初始化输入框
     */
    private fun initEdtViews() {
        mUserNameEdt = bind(R.id.delegate_regist_account_edt)
        mPasswordEdt = bind(R.id.delegate_regist_password_edt)
        mRepasswordEdt = bind(R.id.delegate_regist_repassword_edt)

        mUserNameTextInputLayout = bind(R.id.delegate_regist_account_textInputLayout)
        mPasswordTextInputLayout = bind(R.id.delegate_regist_password_textInputLayout)
        mRepasswordTextInputLayout = bind(R.id.delegate_regist_repassword_textInputLayout)



        mUserNameEdt.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                clearErrorMessage(mUserNameEdt)
            }
        })

        mPasswordEdt.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                clearErrorMessage(mPasswordEdt)
            }
        })

        mRepasswordEdt.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                clearErrorMessage(mRepasswordEdt)
            }
        })

    }


    /**
     * 初始化按钮
     */
    private fun initBtnViews() {
        //---------------------------------注册
        mRegistBtn = bind(R.id.delegate_regist_btn)
        mRegistBtn.setOnClickListener {
            showLoading("正在注册...")
            mPresenter.registAndLogin(mUserNameEdt.text.toString(), mPasswordEdt.text.toString(), mRepasswordEdt.text.toString()) //注册并登录
        }
        //---------------------------------跳过
        mPassBtn = bind(R.id.delegate_regist_pass_btn)
        mPassBtn.setOnClickListener {
            startMainDelegate()
        }
        //---------------------------------跳转登录界面
        mOpenLoginBtn = bind(R.id.delegate_regist_gologin_btn)
        mOpenLoginBtn.setOnClickListener {
            startLoginDelegate()
        }
    }


    /**
     * 清除错误信息
     */
    private fun clearErrorMessage(editText: EditText){
        editText.error = null
    }

    /**
     * 打开登录页面
     */
    private fun startLoginDelegate() {
        val loginDelegate = LoginDelegate()
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            loginDelegate.sharedElementReturnTransition = SharedElementTransation()
            loginDelegate.sharedElementEnterTransition = SharedElementTransation()

            // 25.1.0以下的support包,Material过渡动画只有在进栈时有,返回时没有;
            // 25.1.0+的support包，SharedElement正常

            supportDelegate.extraTransaction()
                    .addSharedElement(mRegistBtn, getString(R.string.transation_login_btn))
                    .startWithPop(loginDelegate)
        } else {
            supportDelegate.startWithPop(loginDelegate)
        }
    }

    /**
     * 打开主界面
     */
    private fun startMainDelegate() {
        supportDelegate.startWithPop(MainDelegate())
    }

    /**
     * 开始加载
     */
    private fun showLoading(loadingText:String){
        getBaseActivity().showLoading(loadingText,false)
    }



    /**
     * 停止加载动画
     */
    private fun stopLoading() {
        getBaseActivity().stopLoading()
    }


    //--------------------------------------------------------------------------对外公开的方法--------------------------------------

    /**
     * 注册成功
     */
    override fun registSuccess() {
        showLoading("正在登录...") //修改显示为正在登录
    }

    /**
     * 注册成功后的操作
     */
    override fun registSuccessAfter() {
        //暂无
    }

    /**
     * 注册失败
     */
    override fun registFail(errorMessage: String) {
        stopLoading()
        ShowToast.error(errorMessage)
    }


    /**
     * 登录成功
     */
    override fun loginSuccess() {
        stopLoading()
        ShowToast.success("登录成功")
    }

    /**
     * 登录成功后的操作
     */
    override fun loginSuccessAfter() {
        startMainDelegate()//打开主界面
    }

    /**
     * 登录失败
     */
    override fun loginFail(errorMessage: String) {
        ShowToast.success("注册成功")
        startMainDelegate()//登录失败照样打开主页面
    }



    /**
     * 输入的账号不合法
     */
    override fun invalidAccount(errorText: String) {
        stopLoading()
        mUserNameEdt.error = errorText
    }

    /**
     * 输入的密码不合法
     */
    override fun invalidPassword(errorText: String) {
        stopLoading()
        mPasswordEdt.error = errorText
    }

    /**
     * 再次输入密码不合法
     */
    override fun invalidRepassword(errorText: String) {
        stopLoading()
        mRepasswordEdt.error = errorText
    }


    override fun setLayout(): Any {
        return R.layout.delegate_regist
    }

}