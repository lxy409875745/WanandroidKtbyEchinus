package com.lxy.wanandroidktbyechinus.ui.welcome

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.util.SparseArray
import android.view.View
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.adapter.WelcomePagerAdapter
import com.lxy.wanandroidktbyechinus.base.BaseQuitFragmentDelegate
import com.lxy.wanandroidktbyechinus.utils.StatusBarCompat
import com.lxy.wanandroidktbyechinus.widget.parallaxscroll.GuidePageChangeListener
import com.lxy.wanandroidktbyechinus.widget.parallaxscroll.ParallaxTransformer
import kotlinx.android.synthetic.main.delegate_welcome.*


class WelcomeDelegate : BaseQuitFragmentDelegate() {
    private val PARALLAX_COEFFICIENT = 1.2f
    private val DISTANCE_COEFFICIENT = 0.8f

    private lateinit var mViewPager:ViewPager



    private lateinit var mPagerAdapter: WelcomePagerAdapter

    private lateinit var idsArray: SparseArray<IntArray>


    override fun onBindView(rootView: View, savedInstanceState: Bundle?) {
        initView()
    }

    private fun initView() {
        initViewpager()

    }
    //-------------------------------------设置viewpager
    private fun initViewpager() {
        mViewPager = bind(R.id.delegate_welcome_viewPager)
        mPagerAdapter = WelcomePagerAdapter(fragmentManager,this)
        mViewPager.adapter = mPagerAdapter
        idsArray = mPagerAdapter.getIds()
        mViewPager.setPageTransformer(true, ParallaxTransformer(PARALLAX_COEFFICIENT, DISTANCE_COEFFICIENT, idsArray))
        mViewPager.addOnPageChangeListener(GuidePageChangeListener(this, mViewPager, mPagerAdapter))
    }
    override fun setLayout(): Any {
        return R.layout.delegate_welcome
    }

    override fun onDestroy() {
        StatusBarCompat.setStatusBarColor(getBaseActivity(),ContextCompat.getColor(context!!,R.color.color_light_blue800))
        super.onDestroy()

    }




}