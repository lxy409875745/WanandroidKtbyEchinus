package com.lxy.wanandroidktbyechinus.widget

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.transition.ChangeBounds
import android.transition.ChangeImageTransform
import android.transition.ChangeTransform
import android.transition.TransitionSet
import android.util.AttributeSet

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
class SharedElementTransation : TransitionSet {

    // 允许资源文件使用
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor() {
        init()
    }

    private fun init() {
        ordering = TransitionSet.ORDERING_TOGETHER
        addTransition(ChangeBounds())
                .addTransition(ChangeTransform())
                .addTransition(ChangeImageTransform())
    }
}
