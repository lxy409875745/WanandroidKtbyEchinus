package com.lxy.wanandroidktbyechinus.widget.parallaxscroll

import android.support.v4.view.ViewPager
import android.os.Build
import android.annotation.TargetApi
import android.R.array
import android.animation.ArgbEvaluator
import android.content.Context
import android.opengl.ETC1.getWidth
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.base.BaseFragmentDelegate


class GuidePageChangeListener(fragmentDelegate: BaseFragmentDelegate, viewPager: ViewPager,pagerAdapter: FragmentPagerAdapter) : ViewPager.OnPageChangeListener {
    var mColorEvaluator: ArgbEvaluator = ArgbEvaluator()
    var mPageWidth: Int = 0
    var mTotalScrollWidth: Int = 0

    var mGuideStartBackgroundColor: Int = 0
    var mGuideEndBackgroundColor: Int = 0


    var mFragment : BaseFragmentDelegate = fragmentDelegate
    var mViewPager :ViewPager = viewPager
    var mAdapter: FragmentPagerAdapter = pagerAdapter


    init {
        mPageWidth = mFragment.getBaseActivity().windowManager.defaultDisplay.width
        mTotalScrollWidth = mPageWidth * mAdapter.getCount()
        mGuideStartBackgroundColor = mFragment.getBaseActivity().getResources().getColor(R.color.color_light_green500)
        mGuideEndBackgroundColor =  mFragment.getBaseActivity().getResources().getColor(R.color.color_light_blue800)
    }


    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        val ratio = (mPageWidth * position + positionOffsetPixels) / mTotalScrollWidth.toFloat()
        val color = mColorEvaluator.evaluate(ratio, mGuideStartBackgroundColor, mGuideEndBackgroundColor) as Int
        mViewPager.setBackgroundColor(color)
    }

    override fun onPageSelected(position: Int) {
    }

    override fun onPageScrollStateChanged(state: Int) {}


}