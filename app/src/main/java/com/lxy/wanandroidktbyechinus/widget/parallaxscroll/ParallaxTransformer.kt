package com.lxy.wanandroidktbyechinus.widget.parallaxscroll

import android.support.v4.view.ViewPager
import android.util.SparseArray
import android.view.View
import com.lxy.wanandroidktbyechinus.R


//viewpager视差滚动
class ParallaxTransformer(private var parallaxCoefficient: Float, private var distanceCoefficient: Float, private var mLayoutViewIdsMap: SparseArray<IntArray>) : ViewPager.PageTransformer {
    override fun transformPage(page: View, position: Float) {
        var scrollXOffset = page.getWidth() * parallaxCoefficient
        var layer:IntArray = mLayoutViewIdsMap.get(mLayoutViewIdsMap.keyAt(0))
        for (id in layer) {
            val view = page.findViewById<View>(id)
            if (view != null) { //title
                if (view.id == R.id.welcome_text_title){
                    view.alpha = 1.0f - Math.abs(position *8)
                    view.translationY= -Math.abs(scrollXOffset * position  )
                }else if (view.id == R.id.welcome_item_login_btn_2 ||view.id == R.id.welcome_item_regist_btn_2 ||view.id == R.id.welcome_item_pass_btn_2){ //隐藏按钮
                    view.alpha = 1.0f - position *5
                }else{
                    view.translationX = scrollXOffset * position
                    view.alpha = 1.0f - position
                }

                scrollXOffset *= distanceCoefficient
            }
        }
    }
}