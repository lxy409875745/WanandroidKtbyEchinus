package com.lxy.wanandroidktbyechinus.widget.banner

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.youth.banner.loader.ImageLoader

/**
 * @ClassName      BannerLoader
 * @Model          todo
 * @Description    用于banner中数据的加载
 * @Author          激烈的海胆
 * @Sign            lxy
 * @Date            2018/9/22 15:57
 */
class BannerImageLoader :ImageLoader() {
    override fun displayImage(context: Context?, path: Any?, imageView: ImageView?) {
        Glide.with(context!!).load(path as String).into(imageView!!)
    }
}