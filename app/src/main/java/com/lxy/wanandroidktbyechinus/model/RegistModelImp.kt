package com.lxy.wanandroidktbyechinus.model

import com.lxy.wanandroidktbyechinus.comment.account.AccountManager
import com.lxy.wanandroidktbyechinus.comment.json.GsonUtils
import com.lxy.wanandroidktbyechinus.comment.network.NetworkHandler
import com.lxy.wanandroidktbyechinus.constract.RegistConstract
import com.lxy.wanandroidktbyechinus.entity.AccountBean

class RegistModelImp :RegistConstract.IRegistModel {

    /**
     * 设置登录状态
     */
    override fun setLoginStatus(isLogin: Boolean) {
        AccountManager.setSignStatus(isLogin)
    }

    /**
     * 保存注册的数据
     */
    override fun saveAccount(account: AccountBean) {
        AccountManager.setAccount(GsonUtils.gson.toJson(account))//保存注册数据
    }


    /**
     * 注册
     */
    override fun regist(userName: String, password: String, rePassword: String) {
        NetworkHandler.regist(userName,password,rePassword)
    }

    /**
     * 登录
     */
    override fun login(userName: String, password: String) {
        NetworkHandler.login(userName,password)
    }
}