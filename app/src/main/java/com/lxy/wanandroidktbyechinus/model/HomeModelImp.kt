package com.lxy.wanandroidktbyechinus.model

import android.content.Context
import com.lxy.wanandroidktbyechinus.constract.ArticleDataBean
import com.lxy.wanandroidktbyechinus.constract.HomeConstract

/**
 * @ClassName      HomeModelImp
 * @Model          todo
 * @Description    todo
 * @Author          激烈的海胆
 * @Sign            lxy
 * @Date            2018/9/21 11:37
 */
class HomeModelImp(context: Context) :HomeConstract.IHomeModel {
    private val mContext:Context = context


    /**
     * 从网络获取首页文章
     */
    override fun requestNetArticle(pageIndex: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    /**
     * 从网络获取首页文章
     */
    override fun requestNetBannerArticle() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }






    /**
     * 从本地获取首页文章
     */
    override fun requestLocalArticle(pageIndex: Int, size: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    /**
     * 从本地获取banner文章
     */
    override fun requestLocalBannerArticle() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    /**
     * 存储文章到本地
     */
    override fun addLocalArticleData(articleData: List<ArticleDataBean.ArticleBean>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * 清除所有本地的首页文章
     */
    override fun clearAllLocalArticleData() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * 获取最早的一条首页文章
     */
    override fun getLocalOldestArticle(): ArticleDataBean.ArticleBean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }





}