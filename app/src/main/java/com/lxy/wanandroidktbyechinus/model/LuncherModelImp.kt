package com.lxy.wanandroidktbyechinus.model

import android.content.Context
import com.lxy.wanandroidktbyechinus.comment.local.SharePreferenceKey
import com.lxy.wanandroidktbyechinus.constract.LuncherContract
import com.lxy.wanandroidktbyechinus.utils.SharePreferenceUtil

class   LuncherModelImp(context: Context?) : LuncherContract.Model {
    private val mContext: Context = context!!


    override fun checkFristOpenStatus(): Boolean {
        var isFristOpen: Boolean = SharePreferenceUtil.put(mContext, SharePreferenceKey.SP_KEY_FRIST_OPEN, true)
        return isFristOpen
    }



    //设置第一次打开标记
    override fun setFristOpenFlag() {
        SharePreferenceUtil.put(mContext,SharePreferenceKey.SP_KEY_FRIST_OPEN,false)
    }

    override fun checkLocalUserData() {

    }

    override fun login() {

    }

}