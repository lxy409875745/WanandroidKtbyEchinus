package com.lxy.wanandroidktbyechinus.model

import android.content.Context
import com.lxy.wanandroidktbyechinus.comment.account.AccountManager
import com.lxy.wanandroidktbyechinus.comment.json.GsonUtils
import com.lxy.wanandroidktbyechinus.comment.network.NetworkHandler
import com.lxy.wanandroidktbyechinus.entity.AccountBean
import com.lxy.wanandroidktbyechinus.constract.LoginConstract

class LoginModelImp(context: Context) : LoginConstract.ILoginModel {


    /**
     * 登录
     */
    override fun login(userName: String, password: String) {
        NetworkHandler.login(userName,password)
    }

    /**
     * 存储登录信息
     */
    override fun saveLoginData(account:AccountBean) {
        AccountManager.setSignStatus(true)
        AccountManager.setAccount(GsonUtils.gson.toJson(account))
    }



}