package com.lxy.wanandroidktbyechinus.entity



data class AccountBean(
    val collectIds: List<Any>,
    val email: String,
    val icon: String,
    val id: Int,
    val password: String,
    val token: String,
    val type: Int,
    val username: String
)