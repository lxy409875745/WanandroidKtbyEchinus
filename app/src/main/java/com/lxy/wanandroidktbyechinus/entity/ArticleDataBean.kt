package com.lxy.wanandroidktbyechinus.constract

import com.chad.library.adapter.base.entity.MultiItemEntity
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * @ClassName ArticleBean
 * @Model todo
 * @Description 首页文章数据
 * @Author 激烈的海胆
 * @Sign lxy
 * @Date 2018/9/21 15:15
 */

class ArticleDataBean {

    var curPage: Int = 0
    var offset: Int = 0
    var isOver: Boolean = false
    var pageCount: Int = 0
    var size: Int = 0
    var total: Int = 0
    var datas: List<ArticleBean>? = null

    @RealmClass //表明使用存储realm的类
    class ArticleBean : RealmModel, MultiItemEntity {
        companion object {
            const val TYPE_NORMAL = 0//正常item
            const val TYPE_PROJECT = 1//工程item
        }

        /**
         * apkLink :
         * author : zqljintu
         * chapterId : 294
         * chapterName : 完整项目
         * collect : false
         * courseId : 13
         * desc : 这是一个简单的练手小项目，项目基于MPV的设计模式打造，我把她命名为记忆胶囊。我记性不好，又不太喜欢记日记，而且对手机自带的备忘录程序不是特别满意，于是萌生了一个这样的想法。
         * 界面是采用的Material design设计语言，模仿的网上的一些界面，加入了一些自己的原创，该App有创建不同事件，日历查找，搜索，界面皮肤等模块。可以满足不同的用户需求。
         * envelopePic : http://wanandroid.com/blogimgs/2ee0f1a3-18b5-44ab-a5f0-1425b9a19ae1.png
         * fresh : false
         * id : 3404
         * link : http://www.wanandroid.com/blog/show/2354
         * niceDate : 2018-09-16
         * origin :
         * projectLink : https://github.com/zqljintu/Memory-capsule
         * publishTime : 1537094095000
         * superChapterId : 294
         * superChapterName : 开源项目主Tab
         * tags : [{"name":"项目","url":"/project/list/1?cid=294"}]
         * title : Memory-capsule（一款备忘录小App记忆胶囊）
         * type : 0
         * userId : -1
         * visible : 1
         * zan : 0
         */
        @PrimaryKey
        var id: Int = 0
        var apkLink: String? = null
        var author: String? = null
        var chapterId: Int = 0
        var chapterName: String? = null
        var isCollect: Boolean = false
        var courseId: Int = 0
        var desc: String? = null
        var envelopePic: String? = null
        var isFresh: Boolean = false

        var link: String? = null
        var niceDate: String? = null
        var origin: String? = null
        var projectLink: String? = null
        var publishTime: Long = 0
        var superChapterId: Int = 0
        var superChapterName: String? = null
        var title: String? = null
        var type: Int = 0
        var userId: Int = 0
        var visible: Int = 0
        var zan: Int = 0
        @Ignore//该字段不存在该表里 需要映射TagsBean
        var tags: List<TagsBean>? = null

        private var itemType: Int = 0//item类型
        fun setItemType(itemType: Int) {
            this.itemType = itemType
        }

        var requestTime :Long = 0//数据请求的时间 用于缓存判断

        override fun getItemType(): Int {
            return this.itemType
        }

        @RealmClass //表明使用存储realm的类
        class TagsBean : RealmModel{
            /**
             * name : 项目
             * url : /project/list/1?cid=294
             */
            @PrimaryKey
            var id: Int = 0

            var name: String? = null
            var url: String? = null
        }
    }
}

