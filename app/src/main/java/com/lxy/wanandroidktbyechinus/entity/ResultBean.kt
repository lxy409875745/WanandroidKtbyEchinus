package com.lxy.wanandroidktbyechinus.entity

//wanandroid服务器返回的结果类型

/**
 * {
"data": {
"collectIds": [],
"email": "",
"icon": "",
"id": 10413,
"password": "lxy19941128",
"token": "",
"type": 0,
"username": "lxy409875745"
},
"errorCode": 0,
"errorMsg": ""
}
 */
open class ResultBean<T> {
    val data: T? = null
    val errorCode: Int = 0
    val errorMsg: String = ""


}
