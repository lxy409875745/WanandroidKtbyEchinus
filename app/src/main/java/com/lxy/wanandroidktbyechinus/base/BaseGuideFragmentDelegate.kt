package com.lxy.wanandroidktbyechinus.base

import com.lxy.wanandroidktbyechinus.R


//欢迎页滑动的fragment
abstract class BaseGuideFragmentDelegate : BaseSingleDelegate() {

    val idsArray: IntArray = intArrayOf(
            R.id.welcome_item_img_0,
            R.id.welcome_item_text_0,
            R.id.welcome_item_img_1,
            R.id.welcome_item_text_1,
            R.id.welcome_item_img_2,
            R.id.welcome_item_text_2,
            R.id.welcome_item_login_btn_2,
            R.id.welcome_item_regist_btn_2,
            R.id.welcome_text_title,
            R.id.welcome_item_pass_btn_2)


    abstract fun getChildViewIds(): IntArray
    abstract fun getFragmentDelegateId(): Int
}