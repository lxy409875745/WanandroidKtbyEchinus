package com.lxy.wanandroidktbyechinus.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.vondear.rxui.view.dialog.RxDialogLoading

abstract class BaseActivity : AppCompatActivity(){
    private lateinit var mLoadingDialogLoading :RxDialogLoading


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mLoadingDialogLoading = RxDialogLoading(this)
    }


    fun showLoading(loadingText:String,canCancel : Boolean){
        mLoadingDialogLoading.setLoadingText(loadingText)
        mLoadingDialogLoading.setCancelable(canCancel)
        mLoadingDialogLoading.show()
    }

    fun showLoading(canCancel : Boolean){
        mLoadingDialogLoading.setLoadingText("")
        mLoadingDialogLoading.setCancelable(canCancel)
        mLoadingDialogLoading.show()
    }

    fun stopLoading(){
        mLoadingDialogLoading.dismiss()
    }
}
