package com.lxy.wanandroidktbyechinus.base

import me.yokeyword.fragmentation.ISupportFragment

abstract class BaseSingleDelegate :BaseFragmentDelegate() {
    lateinit var mParentDelegate : ISupportFragment
    abstract  fun setParentDelegate(parentDelegate : ISupportFragment)//设置父容器fragment 因为未进栈无法通过自身开启新的fragment或关闭fragment 操作需要移步到已经进栈的父容器fragmnet处理
}