package com.lxy.wanandroidktbyechinus.base

import me.yokeyword.fragmentation.anim.FragmentAnimator





//按下回退键直接退出的fragment
abstract class BaseQuitFragmentDelegate: BaseFragmentDelegate() {

    override fun onBackPressedSupport(): Boolean {
        getBaseActivity().finish()
        return true
    }

}