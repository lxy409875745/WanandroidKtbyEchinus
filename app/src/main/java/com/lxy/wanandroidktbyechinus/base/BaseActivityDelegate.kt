package com.lxy.wanandroidktbyechinus.base

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.ContentFrameLayout
import android.util.Log
import android.view.ViewGroup
import com.lxy.wanandroidktbyechinus.R
import com.lxy.wanandroidktbyechinus.utils.StatusBarCompat
import me.yokeyword.fragmentation.ExtraTransaction
import me.yokeyword.fragmentation.ISupportActivity
import me.yokeyword.fragmentation.ISupportFragment
import me.yokeyword.fragmentation.SupportActivityDelegate
import me.yokeyword.fragmentation.anim.FragmentAnimator
import org.greenrobot.eventbus.EventBus
import kotlin.math.log10

abstract class BaseActivityDelegate : BaseActivity(), ISupportActivity {
    private var DELEGATE: SupportActivityDelegate = SupportActivityDelegate(this)
    private lateinit var contentView: ContentFrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        StatusBarCompat.translucentStatusBar(this,false)
        super.onCreate(savedInstanceState)
        DELEGATE.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        // EventBus.getDefault().register(this)
        initDelegateContainer(savedInstanceState)
        onCreat(savedInstanceState)
    }

    @SuppressLint("RestrictedApi")
    private fun initDelegateContainer(savedInstanceState: Bundle?) {
        contentView = ContentFrameLayout(this)
        with(contentView){
            layoutParams =  ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            id = R.id.delegateContainer
        }


        setContentView(contentView)
        if (savedInstanceState == null) {
            DELEGATE.loadRootFragment(contentView.id, setLayout())
        }
    }


    protected abstract fun setLayout(): ISupportFragment//设置容器fragment
    protected abstract fun onCreat(savedInstanceState: Bundle?)

    override fun setFragmentAnimator(fragmentAnimator: FragmentAnimator?) {

        DELEGATE.fragmentAnimator = fragmentAnimator
    }

    override fun getFragmentAnimator(): FragmentAnimator {

        return DELEGATE.fragmentAnimator
    }

    override fun onBackPressedSupport() {
        DELEGATE.onBackPressedSupport()
    }

    override fun extraTransaction(): ExtraTransaction {
        return DELEGATE.extraTransaction()
    }

    override fun onCreateFragmentAnimator(): FragmentAnimator {
        return DELEGATE.onCreateFragmentAnimator()
    }

    override fun getSupportDelegate(): SupportActivityDelegate {
        return DELEGATE
    }

    override fun post(runnable: Runnable?) {
        DELEGATE.post(runnable)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        DELEGATE.onBackPressed()
    }

    override fun onDestroy() {
        DELEGATE.onDestroy()
        super.onDestroy()
        //  EventBus.getDefault().unregister(this)
        System.gc()
        System.runFinalization()
    }

}