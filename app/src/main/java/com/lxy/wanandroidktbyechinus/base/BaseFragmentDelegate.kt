package com.lxy.wanandroidktbyechinus.base

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBarDrawerToggle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lxy.wanandroidktbyechinus.R
import me.yokeyword.fragmentation.ExtraTransaction
import me.yokeyword.fragmentation.ISupportFragment
import me.yokeyword.fragmentation.SupportFragmentDelegate
import me.yokeyword.fragmentation.anim.FragmentAnimator

abstract class BaseFragmentDelegate : BaseFragment(), ISupportFragment {
    private val DELEGATE: SupportFragmentDelegate = SupportFragmentDelegate(this)

    private lateinit var mRootView: View
    private lateinit var mActivity: BaseActivity


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var rootView: View
        var layoutObject: Any = setLayout()
        if (layoutObject is Int) {
            var viewId :Int = layoutObject
            rootView = inflater.inflate(viewId,container,false)
        }else if (layoutObject is View){
            rootView = layoutObject
        }else{
            throw ClassCastException(getString(R.string.exception_classCastException_setLayout))
        }

        mRootView = rootView
        onBindView(mRootView,savedInstanceState)
        return mRootView
    }


    //-----------------------------------------------------------------实现方法
    abstract fun onBindView(rootView :View,@Nullable savedInstanceState: Bundle?)


    abstract fun setLayout(): Any


    //-------------------------------------------------------------------对外公开----------
    open fun <T : View> bind(@IdRes id: Int): T {
        return mRootView.findViewById(id)
    }

    open fun start(fragmentDelegate: BaseFragmentDelegate, lunchMod: Int) {
        DELEGATE.start(fragmentDelegate, lunchMod)
    }

    open fun start(fragmentDelegate: BaseFragmentDelegate) {
        DELEGATE.start(fragmentDelegate)
    }


    /** 获取父辈的fragment
     * @param <T>
     * @return
    </T> */
    fun <T : BaseFragmentDelegate> getParentDelegate(): BaseFragmentDelegate {
        return (parentFragment as T)
    }



    open fun getBaseActivity():BaseActivity{
        return mActivity
    }


    open fun getRootView():View{
        return mRootView
    }



    //----------------------------------------------------------------fragmenation方法

    override fun setFragmentResult(resultCode: Int, bundle: Bundle?) {
        DELEGATE.setFragmentResult(resultCode,bundle)
    }

    override fun onSupportInvisible() {
        DELEGATE.onSupportInvisible()
    }

    override fun onNewBundle(args: Bundle?) {
      DELEGATE.onNewBundle(args)
    }

    override fun extraTransaction(): ExtraTransaction {
       return DELEGATE.extraTransaction()
    }

    override fun onCreateFragmentAnimator(): FragmentAnimator {
        return DELEGATE.onCreateFragmentAnimator()
    }

    override fun enqueueAction(runnable: Runnable?) {
        DELEGATE.enqueueAction(runnable)
    }

    override fun onFragmentResult(requestCode: Int, resultCode: Int, data: Bundle?) {
        DELEGATE.onFragmentResult(requestCode,resultCode,data)
    }

    override fun setFragmentAnimator(fragmentAnimator: FragmentAnimator?) {
      DELEGATE.fragmentAnimator = fragmentAnimator
    }

    override fun onLazyInitView(savedInstanceState: Bundle?) {
       DELEGATE.onLazyInitView(savedInstanceState)
    }

    override fun getFragmentAnimator(): FragmentAnimator {
      return  DELEGATE.fragmentAnimator
    }

    override fun isSupportVisible(): Boolean {
       return  DELEGATE.isSupportVisible
    }

    override fun onEnterAnimationEnd(savedInstanceState: Bundle?) {
        DELEGATE.onEnterAnimationEnd(savedInstanceState)
    }

    override fun onSupportVisible() {
       DELEGATE.onSupportVisible()
    }

    override fun onBackPressedSupport(): Boolean {
        return DELEGATE.onBackPressedSupport()
    }

    override fun getSupportDelegate(): SupportFragmentDelegate {
       return DELEGATE
    }

    override fun putNewBundle(newBundle: Bundle?) {
        DELEGATE.putNewBundle(newBundle)
    }

    override fun post(runnable: Runnable?) {
        DELEGATE.post(runnable)
    }
    //---------------------------------------------fragment----------------------------------------


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DELEGATE.onCreate(savedInstanceState)
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        DELEGATE.onAttach(context as Activity?)
        mActivity = DELEGATE.activity as BaseActivity
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        DELEGATE.onActivityCreated(savedInstanceState)
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        DELEGATE.onSaveInstanceState(outState)
    }

    override fun onResume() {
        super.onResume()
        DELEGATE.onResume()
    }

    override fun onPause() {
        super.onPause()
        DELEGATE.onPause()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        DELEGATE.onDestroyView()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        DELEGATE.onHiddenChanged(hidden)
    }

    override fun onDestroy() {
        super.onDestroy()
        DELEGATE.onDestroy()
    }



}