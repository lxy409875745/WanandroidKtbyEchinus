package com.lxy.wanandroidktbyechinus.base

interface IBasePresenter<T : IBaseView> {
    fun setView(view :T)
    fun initModel()
}